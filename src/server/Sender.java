package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Sender {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Sender Start");

        ServerSocketChannel ssChannel = ServerSocketChannel.open();
        ssChannel.configureBlocking(true);
        int port = 12345;
        ssChannel.socket().bind(new InetSocketAddress(port));

        String obj ="testtext";
        while (true) {
            SocketChannel sChannel = ssChannel.accept();
            if (sChannel != null) {
            	ByteBuffer bt = ByteBuffer.wrap(obj.getBytes());
            	sChannel.write(bt);

            	ByteBuffer buf = ByteBuffer.allocate(10000);

            	int bytesRead = sChannel.read(buf);
            	byte []b = new byte[bytesRead];
            	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
    	        System.out.println("[Client] Got: " + new String(b));
            }
        }
    }
}
