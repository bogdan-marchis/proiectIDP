package server;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class Server {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
    	List<String> name_list = new ArrayList<String>();
        name_list.add("Zoltan");
        name_list.add("Sandor");
        name_list.add("Laszlo");
        name_list.add("Istvan");

        ServerSocketChannel ssChannel = ServerSocketChannel.open();
        ssChannel.configureBlocking(true);
        int port = 12345;
        ssChannel.socket().bind(new InetSocketAddress(port));

        String obj ="testtext";
        while (true) {
            SocketChannel sChannel = ssChannel.accept();
            if (sChannel != null) {

            	ByteBuffer buf = ByteBuffer.allocate(10000);

            	int bytesRead = sChannel.read(buf);
            	byte []b = new byte[bytesRead];
            	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
            	String request = new String(b);
    	        if (request.equals("login")) {
			        String s = "send";
		        	ByteBuffer bt = ByteBuffer.wrap(s.getBytes());
		        	sChannel.write(bt);
		        	
		        	buf = ByteBuffer.allocate(10000);
		        	bytesRead = sChannel.read(buf);
		        	b = new byte[bytesRead];
	            	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
	            	request = new String(b);
	            	boolean found = false;
	            	for (int i = 0; i < name_list.size(); i++) {
	            		if (request.equals(name_list.get(i))) {
	            			found = true;
	            		}
	            	}
	            	if (found) {
	            		s = "accepted";
	            	}
	            	else {
	            		s = "denied";
	            	}
		        	bt = ByteBuffer.wrap(s.getBytes());
		        	sChannel.write(bt);
    	        }
    	        else if (request.equals("message")) {
			        String s = "send";
		        	ByteBuffer bt = ByteBuffer.wrap(s.getBytes());
		        	sChannel.write(bt);
		        	
		        	buf = ByteBuffer.allocate(10000);
		        	bytesRead = sChannel.read(buf);
		        	b = new byte[bytesRead];
	            	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
	            	request = new String(b);
	            	s = "accepted";
		        	bt = ByteBuffer.wrap(s.getBytes());
		        	sChannel.write(bt);
    	        }
            }
        }
    }
}
