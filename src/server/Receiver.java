package server;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Receiver {
    public static void main(String[] args) 
    throws IOException, ClassNotFoundException {
        System.out.println("Receiver Start");

        SocketChannel sChannel = SocketChannel.open();
        sChannel.configureBlocking(true);
        if (sChannel.connect(new InetSocketAddress("localhost", 12345))) {
        	ByteBuffer buf = ByteBuffer.allocate(10000);

        	int bytesRead = sChannel.read(buf);
        	byte []b = new byte[bytesRead];
        	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
	        System.out.println("[Client] Got: " + new String(b));
	        String s = "return mesage";
        	ByteBuffer bt = ByteBuffer.wrap(s.getBytes());
        	sChannel.write(bt);
        }
    }
}
