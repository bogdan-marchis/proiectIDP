package window_mocks;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;

public class Chat {

	protected Shell chat;
	private Text text;
	private String from, to;
	
	public Chat(String from, String to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Chat window = new Chat("from", "to");
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		chat.open();
		chat.layout();
		while (!chat.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void createContents() {
		chat = new Shell();
		chat.setSize(450, 600);
		chat.setText("Alexander Hamilton, Aaron Burr");
		chat.setLayout(null);
		
		final StyledText pastConversation = new StyledText(chat, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP | SWT.MULTI);
		pastConversation.setEditable(false);
		pastConversation.setText("Zoltan:\n\nSir, no man agrees more perfectly than myself to the main principle for which the gentlemen contend.I agree that there should be a broad democratic branch in the national legislature. But this matter, Sir, depends on circumstances; It is impossible, in the first instance to be precise and exact with regard to the number; and it is equally impossible to determine to what point it may be proper in future to increase it. On this ground I am disposed to acquiesce. In my reasonings on the subject of government, I rely more on the interests and the opinions of men, than on any speculative parchment provisions whatever. I have found, that Constitutions are more or less excellent, as they are more or less agreeable to the natural operation of things: I am therefore disposed not to dwell long on curious speculations, or pay much attention to modes and forms; but to adopt a system, whose principles have been sanctioned by experience; adapt it to the real state of our country; and depend on probable reasonings for its operation and result.\n\nSandor:\nAaaand, he's dead.");
		pastConversation.setBounds(5, 5, 435, 402);
		pastConversation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		final StyledText newText = new StyledText(chat, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP | SWT.MULTI);
		newText.setBounds(5, 432, 335, 102);
		newText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Button send = new Button(chat, SWT.NONE);
		send.setBounds(355, 420, 70, 60);
		send.setText("Send");
		send.addListener(SWT.Selection, new Listener()
		{
		    @Override
		    public void handleEvent(Event event)
		    {
		    	pastConversation.setText(pastConversation.getText() + "\nZoltan:\n\n" + newText.getText());
		    	SocketChannel sChannel;
				try {
					sChannel = SocketChannel.open();
			        sChannel.configureBlocking(true);
			        if (sChannel.connect(new InetSocketAddress("localhost", 12345))) {
				        String s = "message";
			        	ByteBuffer bt = ByteBuffer.wrap(s.getBytes());
			        	sChannel.write(bt);

			        	ByteBuffer buf = ByteBuffer.allocate(10000);

			        	int bytesRead = sChannel.read(buf);
			        	byte []b = new byte[bytesRead];
			        	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
			        	String response = new String(b);
			        	if (response.equals("send")) {
			        		s = newText.getText();
			        		bt = ByteBuffer.wrap(s.getBytes());
				        	sChannel.write(bt);

				        	buf= ByteBuffer.allocate(10000);

				        	bytesRead = sChannel.read(buf);
				        	b = new byte[bytesRead];
				        	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
				        	response = new String(b);
				        	if (response.equals("accepted")) {
				        		newText.setText("");
				        	}
			        	}
			        }
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		});
		
		Button upload = new Button(chat, SWT.NONE);
		upload.setBounds(355, 490, 70, 60);
		upload.setText("Upload");
	}
}
