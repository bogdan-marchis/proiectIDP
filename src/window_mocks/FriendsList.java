package window_mocks;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public class FriendsList {

	protected Shell shlFriendsList;
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			FriendsList window = new FriendsList();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlFriendsList.open();
		shlFriendsList.layout();
		while (!shlFriendsList.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	    display.dispose();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlFriendsList = new Shell();
		shlFriendsList.setSize(450, 300);
		shlFriendsList.setText("Friends List");
		
		text = new Text(shlFriendsList, SWT.BORDER);
		text.setBounds(25, 15, 250, 29);
		
		Button addFreind = new Button(shlFriendsList, SWT.NONE);
		addFreind.setBounds(290, 15, 93, 29);
		addFreind.setText("Add Friend");

        final List l = new List(shlFriendsList, SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL);
        l.setBounds(35, 50, 250, 200);
        l.add("Zoltan");
        l.add("Sandor");
        l.add("Laszlo");
        l.add("Istvan");
        
        l.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
            	shlFriendsList.close();
            	Chat chat = new Chat("me", "Sandor");
            	chat.open();
              }
            });
	}

}
