package window_mocks;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class LogIn {

	protected Shell shlLogInWindow;
	private Text username;
	private Text password;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			LogIn window = new LogIn();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlLogInWindow.open();
		shlLogInWindow.layout();
		while (!shlLogInWindow.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlLogInWindow = new Shell();
		shlLogInWindow.setSize(450, 300);
		shlLogInWindow.setText("Log In Window");
		shlLogInWindow.setLayout(null);
		
		username = new Text(shlLogInWindow, SWT.BORDER);
		username.setBounds(140, 60, 190, 40);
		
		password = new Text(shlLogInWindow, SWT.BORDER);
		password.setBounds(140, 95, 190, 40);
		
		Button logInButton = new Button(shlLogInWindow, SWT.PUSH);
		logInButton.setBounds(191, 137, 93, 29);
		logInButton.setText("Log In");
		logInButton.addListener(SWT.Selection, new Listener()
		{
		    @Override
		    public void handleEvent(Event event)
		    {

		        SocketChannel sChannel;
				try {
					sChannel = SocketChannel.open();
			        sChannel.configureBlocking(true);
			        if (sChannel.connect(new InetSocketAddress("localhost", 12345))) {
				        String s = "login";
			        	ByteBuffer bt = ByteBuffer.wrap(s.getBytes());
			        	sChannel.write(bt);
			        	
			        	ByteBuffer buf = ByteBuffer.allocate(10000);

			        	int bytesRead = sChannel.read(buf);
			        	byte []b = new byte[bytesRead];
			        	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
			        	String response = new String(b);
			        	if (response.equals("send")) {
			        		s = username.getText();
			        		bt = ByteBuffer.wrap(s.getBytes());
				        	sChannel.write(bt);

				        	buf= ByteBuffer.allocate(10000);

				        	bytesRead = sChannel.read(buf);
				        	b = new byte[bytesRead];
				        	System.arraycopy(buf.array(), 0, b, 0, bytesRead);
				        	response = new String(b);
				        	if (response.equals("accepted")) {
				        		shlLogInWindow.close();
				        		FriendsList friendsList = new FriendsList();
				        		friendsList.open();
				        	}
			        	}
			        }
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		});

	}
}
